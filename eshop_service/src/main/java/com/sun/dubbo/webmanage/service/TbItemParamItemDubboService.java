package com.sun.dubbo.webmanage.service;

import com.sun.webmanage.model.TbItemParamItem;

public interface TbItemParamItemDubboService {

	boolean insertTbItemParamItem(TbItemParamItem record);
	
	TbItemParamItem selectTbItemParamItemByItemId(long item_id);
	
}
