package com.sun.dubbo.webmanage.service;

import com.sun.webmanage.model.TbOrderShipping;

public interface TbOrderShippingDubboService {

	boolean insertTbOrderShipping(TbOrderShipping tbOrderShipping);
	
	boolean updateTbOrderShipping(TbOrderShipping tbOrderShipping);
	
}
