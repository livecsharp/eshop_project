package com.sun.dubbo.webmanage.service;

import java.util.List;

import com.sun.commons.pojo.EasyUIDataGrid;
import com.sun.webmanage.model.TbContent;

public interface TbContentDubboService {

	EasyUIDataGrid listTbContentPage(int pager,int rows,long cid);
	
	Long saveTbContent(TbContent tbContent);
	
	boolean editTbContent(TbContent tbContent);
	
	boolean deleteTbContent(long id);
	
	
	/**
	 * 网站内容
	 * @param limit 显示前几个  等于0表示查询全部
	 * @param is_sort 是否排序  更新时间倒叙  最新的显示在最前
	 * @return
	 */
	List<TbContent> listTbContentLimit(long category_id,int limit,boolean is_sort);
	
}
