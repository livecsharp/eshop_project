package com.sun.dubbo.webmanage.service;

import com.sun.webmanage.model.TbItemDesc;

public interface TbItemDescDubboService {

	TbItemDesc selectTbItemDescByItemId(long item_id);
}
