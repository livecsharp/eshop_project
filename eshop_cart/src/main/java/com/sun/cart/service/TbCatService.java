package com.sun.cart.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.sun.commons.pojo.TbItemVO;

public interface TbCatService {

	boolean addTbItemInCart(Long item_id,int num,HttpServletRequest request);
	
	List<TbItemVO> listCartTbitem(HttpServletRequest request);
	
	boolean updateTbItemCart(Long item_id,int num,HttpServletRequest request);
	
	boolean deleteTbItemCart(String itemId,HttpServletRequest request);
	
}
