var TTCart = {
	load : function(){ // 加载购物车数据
		
	},
	itemNumChange : function(){
		$(".increment").click(function(){//＋
			var _thisInput = $(this).siblings("input");
			_thisInput.val(eval(_thisInput.val()) + 1);
			$.post("/cart/update/num/"+_thisInput.attr("itemId")+"/"+_thisInput.val() + ".action",function(data){
				TTCart.refreshTotalPrice();
				//刷新数量
				TTCart.refreshTotalNum();
			});
		});
		$(".decrement").click(function(){//-
			var _thisInput = $(this).siblings("input");
			if(eval(_thisInput.val()) == 1){
				return ;
			}
			_thisInput.val(eval(_thisInput.val()) - 1);
			$.post("/cart/update/num/"+_thisInput.attr("itemId")+"/"+_thisInput.val() + ".action",function(data){
				TTCart.refreshTotalPrice();
				//刷新数量
				TTCart.refreshTotalNum();
			});
		});
		$(".quantity-form .quantity-text").rnumber(1);//限制只能输入数字
		$(".quantity-form .quantity-text").change(function(){
			var _thisInput = $(this);
			$.post("/service/cart/update/num/"+_thisInput.attr("itemId")+"/"+_thisInput.val(),function(data){
				TTCart.refreshTotalPrice();
				//刷新数量
				TTCart.refreshTotalNum();
			});
		});
	},
	refreshTotalPrice : function(){ //重新计算总价
		var total = 0;
		$(".quantity-form .quantity-text").each(function(i,e){
			var _this = $(e);
			if(_this.parent().parent().siblings(".p-checkbox").children().eq(0)[0].checked){
			total += (eval(_this.attr("itemPrice")) * 10000 * eval(_this.val())) / 10000;
			}
		});
		$(".totalSkuPrice").html(new Number(total/100).toFixed(2)).priceFormat({ //价格格式化插件
			 prefix: '￥',
			 thousandsSeparator: ',',
			 centsLimit: 2
		});
	},
	refreshTotalNum : function(){//重新计算数量
		var totalNum = 0;
		$(".quantity-form .quantity-text").each(function(i,e){
			var _this = $(e);
			if(_this.parent().parent().siblings(".p-checkbox").children().eq(0)[0].checked){
				totalNum += eval(_this.val());
			}
		});
		$("#selectedCount").text(totalNum);
	}
	
};

$(function(){
	TTCart.load();
	TTCart.itemNumChange();
	TTCart.refreshTotalNum();//刷新数量
	
	//点击删除选中的商品链接
	$("#remove-batch").click(function(e){
		var itemIdArr = [];
		$(".quantity-form .quantity-text").each(function(i,e){
			var _this = $(e);
			if(_this.parent().parent().siblings(".p-checkbox").children().eq(0)[0].checked){
				itemIdArr.push(_this.attr("itemId"));
				_this.parent().parent().parent().parent().remove();
			}
		});
		
		$.post('/cart/delete/'+itemIdArr.join(",")+'.action',function(data){
			if(data.status==200){
				TTCart.refreshTotalPrice();
				//刷新数量
				TTCart.refreshTotalNum();
			}
		});
		
	});
	
	//对删除超链接添加点击事情
	$(".mycart_remove").click(function(){
		var $a =$(this);
		var href=$(this).attr("href");
		$.post(href,function(data){
			if(data.status==200){
				//parent()当前标签的父标签
				$a.parent().parent().parent().remove();
				TTCart.refreshTotalPrice();
				//刷新数量
				TTCart.refreshTotalNum();
			}
		})
		return false;
	});
	//对复选框添加点击事件
	$(".checkbox").click(function(){
		var total = 0;
		$(".quantity-form .quantity-text").each(function(i,e){
			var _this = $(e);
			if(_this.parent().parent().siblings(".p-checkbox").children().eq(0)[0].checked){
				total += (eval(_this.attr("itemPrice")) * 10000 * eval(_this.val())) / 10000;
			}
		});
		$(".totalSkuPrice").html(new Number(total/100).toFixed(2)).priceFormat({ //价格格式化插件
			 prefix: '￥',
			 thousandsSeparator: ',',
			 centsLimit: 2
		});
		//刷新数量
		TTCart.refreshTotalNum();
		//操作全选按钮
		var _status = $(this).prop("checked");
		if(_status){
			var single_checkbox = $(".checkbox");
			var _flag = true;
			single_checkbox.each(function(i,e){
				if(!$(e).prop("checked")){
					_flag = false;
				}
			});
			if(_flag){
				openAllBox();
			}
		}else{
			closeAllBox();
		}
	});
	//"去结算"按钮点击事件
	$("#toSettlement").click(function(){
		//alert($(".checkbox:checked").length);
		//i脚标   n当前循环时对象,对象是一个dom对象
		var param = "";
		$.each($(".checkbox:checked"),function(i,n){
			//alert($(n).val());
			param+="id="+$(n).val();
			if(i<$(".checkbox:checked").length-1){
				param+="&";
			}
		});
		//alert(param);
		if(param==null || param==""){
			alert("请选择需要结算的商品");
			return false;
		}
		location.href=$(this).attr("href")+"?"+param;
		return false;
	});
	
	//全选按钮
	//toggle-checkboxes_down toggle-checkboxes_up
	$("#toggle-checkboxes_up,#toggle-checkboxes_down").change(function(e){
		var status = $(this).prop("checked");
		var single_checkbox = $(".checkbox");
		if(status){
			single_checkbox.each(function(i,e){
				$(e).attr("checked",true);
				openAllBox();
			});
		}else{
			single_checkbox.each(function(i,e){
				$(e).attr("checked",false);
				closeAllBox();
			});
		}
		
		TTCart.refreshTotalPrice();
		TTCart.refreshTotalNum();//刷新数量
	});
	
	//开启上下全选
	function openAllBox(){
		$("#toggle-checkboxes_up").attr("checked",true);
		$("#toggle-checkboxes_down").attr("checked",true);
	}
	//关闭上下全选
	function closeAllBox(){
		$("#toggle-checkboxes_up").attr("checked",false);
		$("#toggle-checkboxes_down").attr("checked",false);
	}
	
});