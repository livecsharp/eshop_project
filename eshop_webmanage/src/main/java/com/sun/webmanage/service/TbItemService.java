package com.sun.webmanage.service;

import java.util.Map;

import com.sun.commons.pojo.EasyUIDataGrid;
import com.sun.exception.DaoException;
import com.sun.webmanage.model.TbItem;
import com.sun.webmanage.model.TbItemDesc;
import com.sun.webmanage.model.TbItemParamItem;

public interface TbItemService {

	EasyUIDataGrid listTbItemPage(int page,int rows,String sort,String order);
	
	boolean editStatusTbItem(String ids,String status) throws DaoException;
	
	Map<String,Object> saveTbItemRelatoinInfo(TbItem tbItem,String desc,String paramData)throws DaoException;

    TbItemDesc loadTbItemDescByItemId(long item_id);
    
    TbItemParamItem loadTbItemParamItemByItemId(long item_id);
    
    Map<String,Object> updateTbItemRelationInfo(TbItem tbItem,String desc,String itemParams,Long itemParamId);

}
