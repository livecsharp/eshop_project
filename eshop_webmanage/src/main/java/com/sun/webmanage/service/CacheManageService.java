package com.sun.webmanage.service;

import java.util.Map;

public interface CacheManageService {

	Map<String,Object> reflushSolrCache(boolean is_all);
	
}
