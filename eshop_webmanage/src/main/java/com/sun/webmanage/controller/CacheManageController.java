package com.sun.webmanage.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.webmanage.service.CacheManageService;

@Controller
public class CacheManageController {

	@Resource
	private CacheManageService cacheManageService;
	
	@ResponseBody
	@RequestMapping("/reflash/solrcache/{is_all}")
	public Map<String,Object> reflashSolrCache(@PathVariable boolean is_all){
		return cacheManageService.reflushSolrCache(is_all);
	}
}
