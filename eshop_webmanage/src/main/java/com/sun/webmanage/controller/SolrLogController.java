package com.sun.webmanage.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.commons.pojo.EasyUIDataGrid;
import com.sun.webmanage.service.SolrLogService;

@Controller
public class SolrLogController {

	@Resource
	private SolrLogService solrLogService;
	
	@ResponseBody
	@RequestMapping("solr/list")
	public EasyUIDataGrid solrList(Integer page,Integer rows){
		EasyUIDataGrid listdata = solrLogService.listSolrLog(page, rows);
		return listdata;
	}
}
