package com.sun.dubbo.webmanage.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sun.commons.pojo.EasyUIDataGrid;
import com.sun.dubbo.webmanage.service.TbItemDubboService;
import com.sun.exception.DaoException;
import com.sun.webmanage.mapper.CustomTbItemMapper;
import com.sun.webmanage.mapper.TbItemCatMapper;
import com.sun.webmanage.mapper.TbItemDescMapper;
import com.sun.webmanage.mapper.TbItemMapper;
import com.sun.webmanage.mapper.TbItemParamItemMapper;
import com.sun.webmanage.model.TbItem;
import com.sun.webmanage.model.TbItemDesc;
import com.sun.webmanage.model.TbItemExample;
import com.sun.webmanage.model.TbItemParamItem;
import com.sun.webmanage.model.TbItemParamItemExample;

public class TbItemDubboServiceImpl implements TbItemDubboService {

	@Resource
	private TbItemMapper tbItemMapper;
	@Resource
	private TbItemDescMapper tbItemDescMapper;
	@Resource
	private TbItemParamItemMapper tbItemParamItemMapper;
	@Resource
	private TbItemCatMapper tbItemCatMapper;
	
	@Resource
	private CustomTbItemMapper customTbItemMapper;

	@Override
	public EasyUIDataGrid listTbItem(int page, int rows, String sort, String order) {
		PageHelper.startPage(page, rows);
		TbItemExample tbItemExample = new TbItemExample();
		if (sort != null && order != null) {
			tbItemExample.setOrderByClause(sort + " " + order);
		}
		List<TbItem> list = customTbItemMapper.selectItemAndCatByExample(tbItemExample);
		PageInfo<TbItem> info = new PageInfo<>(list);
		EasyUIDataGrid dataGrid = new EasyUIDataGrid(info.getList(), info.getTotal());
		return dataGrid;
	}

	@Override
	public boolean updateStatusTbItem(String ids, Byte status) throws DaoException {
		String[] del_ids = ids.split(",");
		TbItem item = new TbItem();
		item.setStatus(status);
		for (String id : del_ids) {
			int affectedLine;
			try {
				item.setId(Long.parseLong(id));
				affectedLine = tbItemMapper.updateByPrimaryKeySelective(item);
				if (affectedLine <= 0) {
					throw new DaoException("更新商品状态异常：更新失败");
				}
			} catch (NumberFormatException e) {
				throw new DaoException("更新商品状态异常：" + e.getMessage());
			}
		}
		return true;
	}

	@Override
	public boolean insertTbItemAndDesc(TbItem tbItem, TbItemDesc tbItemDesc, TbItemParamItem tbItemParamItem)
			throws DaoException {
		try {
			if (tbItemMapper.insert(tbItem) > 0) {
				if (tbItemDescMapper.insertSelective(tbItemDesc) > 0) {
					if (tbItemParamItemMapper.insertSelective(tbItemParamItem) > 0) {
						return true;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DaoException("保存商品信息异常：" + e.getMessage());
		}
		return false;
	}

	@Override
	public List<TbItem> selectAllByStatusCreateTime(byte status, Date startDateTime, Date endDateTime) {
		TbItemExample example = new TbItemExample();
		example.createCriteria().andStatusEqualTo(status).andCreatedBetween(startDateTime, endDateTime);
		return tbItemMapper.selectByExample(example);
	}

	@Override
	public boolean updateItemRelationInfo(TbItem tbItem, TbItemDesc tbItemDesc, TbItemParamItem tbItemParamItem) {
		if (tbItemMapper.updateByPrimaryKeySelective(tbItem) <= 0) {
			return false;
		}
		if (tbItemDescMapper.updateByPrimaryKeySelective(tbItemDesc) <= 0) {
			return false;
		}
		TbItemParamItemExample example = new TbItemParamItemExample();
		example.createCriteria().andItemIdEqualTo(tbItem.getId());
		List<TbItemParamItem> loadtbItemParamItem = tbItemParamItemMapper.selectByExample(example);
		if (loadtbItemParamItem != null && !loadtbItemParamItem.isEmpty()) {
			if (tbItemParamItemMapper.updateByPrimaryKeySelective(tbItemParamItem) <= 0) {
				return false;
			}
		}

		return true;
	}

	@Override
	public TbItem selectTbItemByPK(Long id) {
		TbItemExample example = new TbItemExample();
		example.createCriteria().andIdEqualTo(id);
		return tbItemMapper.selectByExample(example).get(0);
	}

	@Override
	public boolean updateItem(TbItem tbItem) {
		if(tbItemMapper.updateByPrimaryKeySelective(tbItem)>0){
			return true;
		}
		return false;
	}

}
