package com.sun.search.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.stereotype.Service;

import com.sun.commons.pojo.TbItemVO;

@Service("ItemSearchService")
public class ItemSearchServiceImpl implements ItemSearchService{

	@Resource
	private CloudSolrClient cloudSolrClient;

	@Override
	public Map<String, Object> tbItemSearch(int page, int rows, String query) throws SolrServerException, IOException {
		SolrQuery params = new SolrQuery();
		//设置分页
		params.setStart((page-1)*rows);
		params.setRows(rows);
		//设置查询条件
		params.setQuery("item_keywords:"+query);
		//设置高亮
		params.setHighlight(true);
		params.addHighlightField("item_title");
		params.setHighlightSimplePre("<span style='color:red;'>");
		params.setHighlightSimplePost("</span>");
		
		QueryResponse result = cloudSolrClient.query(params);
		SolrDocumentList response = result.getResults();
		Map<String, Map<String, List<String>>> highlighting = result.getHighlighting();
	
		List<TbItemVO> listdata = new ArrayList<>(rows);
		TbItemVO item = null;
		for(SolrDocument doc:response){
			item = new TbItemVO();
			item.setId(Long.parseLong(doc.getFieldValue("id").toString()));
			Map<String, List<String>> highlight = highlighting.get(doc.getFieldValue("id"));
			List<String> hightList = highlight.get("item_title");
			if(hightList!=null && !hightList.isEmpty()){
				item.setTitle(hightList.get(0));
			}else{
				item.setTitle(doc.getFieldValue("item_title").toString());
			}
			item.setSellPoint(doc.getFieldValue("item_sell_point").toString());
			item.setPrice(Long.parseLong(doc.getFieldValue("item_price").toString()));
			item.setImages(doc.getFieldValue("item_image")==null?new String[1]:doc.getFieldValue("item_image").toString().split(","));
			
			listdata.add(item);
		}
		
		Map<String,Object> retmap = new HashMap<>();
		retmap.put("itemList", listdata);
		retmap.put("query", query);
		retmap.put("count", response.getNumFound());
		retmap.put("totalPages", response.getNumFound()%rows==0?response.getNumFound()/rows:(response.getNumFound()/rows)+1);
		retmap.put("page", page);	
		
		return retmap;
	}

	@Override
	public Map<String, Object> tbItemSearchByCatName(int page, int rows, String catname)
			throws SolrServerException, IOException {
		SolrQuery params = new SolrQuery();
		//设置分页
		params.setStart((page-1)*rows);
		params.setRows(rows);
		//设置查询条件
		params.setQuery("item_category_name:"+catname);
		//设置高亮
		params.setHighlight(true);
		params.addHighlightField("item_title");
		params.setHighlightSimplePre("<span style='color:red;'>");
		params.setHighlightSimplePost("</span>");
		
		QueryResponse result = cloudSolrClient.query(params);
		SolrDocumentList response = result.getResults();
		Map<String, Map<String, List<String>>> highlighting = result.getHighlighting();
	
		List<TbItemVO> listdata = new ArrayList<>(rows);
		TbItemVO item = null;
		for(SolrDocument doc:response){
			item = new TbItemVO();
			item.setId(Long.parseLong(doc.getFieldValue("id").toString()));
			Map<String, List<String>> highlight = highlighting.get(doc.getFieldValue("id"));
			List<String> hightList = highlight.get("item_title");
			if(hightList!=null && !hightList.isEmpty()){
				item.setTitle(hightList.get(0));
			}else{
				item.setTitle(doc.getFieldValue("item_title").toString());
			}
			item.setSellPoint(doc.getFieldValue("item_sell_point").toString());
			item.setPrice(Long.parseLong(doc.getFieldValue("item_price").toString()));
			item.setImages(doc.getFieldValue("item_image")==null?new String[1]:doc.getFieldValue("item_image").toString().split(","));
			
			listdata.add(item);
		}
		
		Map<String,Object> retmap = new HashMap<>();
		retmap.put("itemList", listdata);
		retmap.put("query", catname);
		retmap.put("count", response.getNumFound());
		retmap.put("totalPages", response.getNumFound()%rows==0?response.getNumFound()/rows:(response.getNumFound()/rows)+1);
		retmap.put("page", page);	
		
		return retmap;
	}
	
	
	
}
